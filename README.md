ADBridge Realm
==============

ADBridge Realm project is developed for Tomcat container to query LDAP for user authentication through the ADBridge API.
See [adbridge](https://gitlab.com/ucsdlibrary/development/lib-adbridge).

Environment
-----------
* Java 8
* Apache Ant
* Tomcat 7+

Build
-----
Clone the project and move to the project root directory adbridge-realm/, then run:

```
ant
```
The distribute jar is located at directory dist/.

Run Test
--------
```
ant test
```

Deploy
------
Copy dist/adbridge-realm-0.1.jar to the Tomcat lib/ directory.


Configuration
-------------
Edit server.xml (or context.xml) and replace the JNDIRealm configuration (if exists):

```
<Realm className="edu.ucsd.library.security.ADBridgeRealm" connectionURL="https://lib-adbridge.ucsd.edu" roleBase="OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU" />
```
* connectionURL: The connection URL for the ADBridge server that Tomcat will contact.
* roleBase: The base element for role searches.

Licenses
--------
* ADBridge Realm is governed by the [UC Copyright Notice](UC_Copyright_Notice.txt).
* See [third-party licensing](third-party-licenses.txt) for more information on licenses for
  Java dependencies bundled with ADBridge Realm.
