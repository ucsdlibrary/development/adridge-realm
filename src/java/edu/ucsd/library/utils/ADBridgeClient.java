package edu.ucsd.library.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Class ADBridgeClient provides utility functions to query ADBridge API
 * @author lsitu
 * @since 01/2024
 */
public class ADBridgeClient {
    public static final String POST_METHOD = "POST";
    public static final String GET_METHOD = "GET";

    public static final String ROLE_NAME_KEY = "samAccountName";
    public static final String DISTINGUISHED_NAME_KEY = "distinguishedName";
    private static Pattern roleGroupPattern = Pattern.compile("(\\{([^}]*)\\})");
    private static Pattern rolePattern = Pattern.compile("(\"" + ROLE_NAME_KEY + "\":\"([^\"]*)\")");
    private static Pattern roleBasePattern = Pattern.compile("(\"" + DISTINGUISHED_NAME_KEY + "\":\"([^\"]*)\")");

    private final String connectionURL;

    public ADBridgeClient(String connectionURL)
            throws KeyManagementException, NoSuchAlgorithmException {
        this.connectionURL = connectionURL;

        initSSLContext();
    }

    /**
     * Retrieve user roles from adbridge api
     * @param user - The user id/samAccountName
     * @param credentials - The user password
     * @param roleBase - The base element for role searches.
     * @return
     * @throws Exception
     */
    public List<String> retrieveUserRoles(String user, String credentials, String roleBase) throws Exception {
        return retrieveUserRoles(user, user, credentials, roleBase);
    }

    /**
     * Retrieve roles for user from adbridge api
     * @param user - The user id/samAccountName
     * @param connectionName - The connection name for the server we will contact.
     * @param connectionPassword - The connection password for the server we will contact.
     * @param roleBase - The base element for role searches.
     * @return
     * @throws Exception
     */
    public List<String> retrieveUserRoles(String user, String connectionName, String connectionPassword, String roleBase) throws Exception {
        // GET /UserMembership/{id}
        String membershipsUrl = connectionURL + "/UserMembership/" + user;
        String groups = httpGet(membershipsUrl, connectionName, connectionPassword);

        return extractUserRoles(groups, roleBase);
    }

    /**
     * Retrieve user info from adbridge api
     * @param user - The user id/samAccountName
     * @param connectionName - The connection name for the server we will contact.
     * @param connectionPassword - The connection password for the server we will contact.
     * @return JSON
     * @throws Exception
     */
    public String retrieveUserInfo(String user, String connectionName, String connectionPassword) throws Exception {
        // GET /UserInfo/{id}
        String userInfoUrl = connectionURL + "/UserInfo/" + user;
        return httpGet(userInfoUrl, connectionName, connectionPassword);
    }

    /**
     * Check whether user name and credentials matched or not
     * @param user - The user id/samAccountName
     * @param credentials - The user's password
     * @return
     * @throws Exception 
     */
    public List<String> checkCredentials(String user, String credentials) throws Exception {
        return retrieveUserRoles(user, credentials, null);
    }

    /**
     * Send http GET request with basic authentication
     * @param url
     * @param connectionName
     * @param connectionPassword
     * @return
     * @throws Exception
     */
    protected String httpGet(String url, String connectionName, String connectionPassword) throws Exception {
        return sendRequestWithAuthHeader(url, GET_METHOD, connectionName, connectionPassword);
    }

    /**
     * Send http POST request with basic authentication
     * @param url
     * @param method
     * @param connectionName
     * @param connectionPassword
     * @return
     * @throws Exception
     */
    protected String httpPost(String url, String connectionName, String connectionPassword) throws Exception {
        return sendRequestWithAuthHeader(url, POST_METHOD, connectionName, connectionPassword);
    }

    /**
     * Send http request with basic authentication
     * @param url
     * @param method
     * @param connectionName
     * @param connectionPassword
     * @return
     * @throws Exception
     */
    protected String sendRequestWithAuthHeader(String url, String method, String connectionName, String connectionPassword) throws Exception {
        HttpURLConnection connection = null;
        BufferedReader in = null;
        try {
            connection = createConnection(url, method);
            connection.setRequestProperty("Authorization", createBasicAuthHeaderValue(connectionName, connectionPassword));
            int statusCode = connection.getResponseCode();

            if (statusCode > 299) {
                in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
            StringBuffer sb = new StringBuffer();
            String s = "";
            while ((s = in.readLine()) != null) {
                sb.append(s + "\n");
            }

            if (statusCode > 299) {
                throw new Exception("Error http response from " + url + ": " + sb.toString());
            }

            return sb.toString();
        } finally {
            if (in != null) {
                in.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /*
     * Create HttpURLConnection for http request handling
     * @param urlString
     * @param method
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    private HttpURLConnection createConnection(String urlString, String method)
            throws MalformedURLException, IOException {
        URL url = new URL(String.format(urlString));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        return connection;
    }

    /*
     * Create value for basic authentication
     * @param user
     * @param credentials
     * @return
     */
    private String createBasicAuthHeaderValue(String connectionName, String connectionPassword) {
        String auth = connectionName + ":" + connectionPassword;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
        String authHeaderValue = "Basic " + new String(encodedAuth);
        return authHeaderValue;
    }

    /**
     * Setup SSLContext for https connections
     * @throws NoSuchAlgorithmException 
     * @throws KeyManagementException 
     */
    private void initSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        // Accept all SSL certificates
        X509TrustManager trustManager = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException { }
            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException { }
            public X509Certificate[] getAcceptedIssuers() { return null; }
        };

        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(null, new TrustManager[] { trustManager }, new java.security.SecureRandom());
        // Set default SSLSocketFactory
        HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());

        HostnameVerifier hostsValid = new HostnameVerifier() {
            @Override
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };
        // Set the trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostsValid);
    }

    /**
     * Extract user roles from groups in JSON format
     * @example: [{"DistinguishedName":"CN=dams-manager-admin,OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU","samAccountName":"dams-manager-admin","GID":"10000001", ...}, {"DistinguishedName":"CN=dams-curator,OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU","samAccountName":"dams-curator","GID":"10000002", ...}]
     * @see https://lib-adbridge.ucsd.edu/swagger/index.html#operations-tag-UserMembership
     * 
     * @param groups
     * @param roleBase - The base element for role searches.
     * @return
     */
    public List<String> extractUserRoles(String groups, String roleBase) {
        List<String> roles = new ArrayList<String>();

        //Tomcat 7 doesn't include JSONParser. We use regular expression to extract the user roles instead.
        //Matching the compiled pattern in the String
        Matcher roleGrouMatcher = roleGroupPattern.matcher(groups);
        while (roleGrouMatcher.find()) {
            String roleGroup = roleGrouMatcher.group(1);

            // Match role base pattern if roleBase search filter presents
            if (roleBase != null && roleBase.length() > 0) {
                Matcher roleBaseMatcher = roleBasePattern.matcher(roleGroup);
                if (!roleBaseMatcher.find() || roleBaseMatcher.group(2).indexOf(roleBase) < 0) {
                    continue;
                }
            }

            // Extract role from the roleGroup JSON.
            Matcher roleMatcher = rolePattern.matcher(roleGroup);
            if (roleMatcher.find()) {
                roles.add(roleMatcher.group(2));
            }
        }

        return roles;
    }

}
