package edu.ucsd.library.security;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

import edu.ucsd.library.utils.ADBridgeClient;

/**
 * Custom Realm to use ADBridge API for Tomcat container authentication
 * 
 * @example Tomcat configuration in server.xml, which need to replace the existing JNDIRealm:
 * <Realm className="edu.ucsd.library.security.ADBridgeRealm" connectionURL="https://lib-adbridge.ucsd.edu" />
 *
 * @author lsitu
 * @since 01/2024
 */
public class ADBridgeRealm extends RealmBase {
    private static ADBridgeClient _adBridgeClient = null;

    private String userName;
    private String password;

    // The connection URL for the server we will contact.
    protected String connectionURL = null;
    // The base element for role searches.
    protected String roleBase = "";

    @Override
    protected String getName() {
        return userName;
    }

    @Override
    protected String getPassword(String arg0) {
        return password;
    }

    /**
     * Return the base element for role searches.
     */
    public String getRoleBase() {
        return (this.roleBase);
    }

    /**
     * Set the base element for role searches.
     *
     * @param roleBase The new base element
     */
    public void setRoleBase(String roleBase) {
        this.roleBase = roleBase;
    }

    /**
     * Return the connection URL for this Realm.
     */
    public String getConnectionURL() {
        return (this.connectionURL);
    }

    /**
     * Set the connection URL for this Realm.
     * @param connectionURL The new connection URL
     */
    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }

    @Override
    public Principal authenticate(String username, String credentials) {
        this.userName = username;
        this.password = credentials;

        if (username == null || username.equals("") || credentials == null || credentials.equals("")) {
            if (containerLog.isDebugEnabled()) {
                containerLog.debug("username null or empty: returning null principal.");
            }
            return (null);
        }

        try {
            return getPrincipal(userName);
        } catch (Exception ex) {
            containerLog.error("Error generate principal for user " + username, ex);
        }
        return (null);
    }

    @Override
    protected Principal getPrincipal(String user) {
        List<String> roles = new ArrayList<String>();

        // Retrieve user roles from adbridge api
        try {
            roles = getADBridgeClient().retrieveUserRoles(user, password, roleBase);

            containerLog.info("Retrieve roles for user " + userName + ".");

        } catch (Exception e) {
            containerLog.error("Error authenticate user " + user, e);
            return (null);
        }

        Principal principal = new GenericPrincipal(userName, password, roles);

        return principal;
    }

    /*
     * Create the singleton ADBridgeClient instance
     * @return
     */
    private synchronized ADBridgeClient getADBridgeClient() throws KeyManagementException, NoSuchAlgorithmException {
        if (_adBridgeClient == null) {
            containerLog.info("AdBridge URL base: " + connectionURL);

            _adBridgeClient = new ADBridgeClient(connectionURL);
        }
 
        return _adBridgeClient;
    }
}
