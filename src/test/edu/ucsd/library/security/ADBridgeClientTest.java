package edu.ucsd.library.security;

import static org.junit.Assert.assertArrayEquals;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.ucsd.library.utils.ADBridgeClient;
import junit.framework.TestCase;

public class ADBridgeClientTest extends TestCase {
    private static String roleBase = "OU=Application Groups,OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU";
    private static String groupsJson = "[{\"distinguishedName\":\"CN=dams-manager-admin,OU=Application Groups,OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU\",\"GID\":\"10000001\",\"samAccountName\":\"dams-manager-admin\"}, {\"distinguishedName\":\"CN=dams-curator,OU=Groups,OU=University Library,DC=AD,DC=UCSD,DC=EDU\", \"GID\":\"10000002\",\"samAccountName\":\"dams-curator\"}]";

    private ADBridgeClient adBridgeClient;

    @BeforeClass
    public static void init() {
    }

    @Test
    public void testExtractUserRoles() throws Exception {
        // initiate role pattern
        adBridgeClient = new ADBridgeClient("https://lib-adbridge.ucsd.edu");

        List<String> actualRoles = adBridgeClient.extractUserRoles(groupsJson, null);

        String[] expectedRoles = {"dams-manager-admin", "dams-curator"};
        assertArrayEquals("Wrong roles extracted: " + String.join(", ", actualRoles), expectedRoles, actualRoles.toArray());
    }

    @Test
    public void testExtractUserRolesWithRoleBaseSearch() throws Exception {
        // initiate role pattern
        adBridgeClient = new ADBridgeClient("https://lib-adbridge.ucsd.edu");

        List<String> actualRoles = adBridgeClient.extractUserRoles(groupsJson, roleBase);

        String[] expectedRoles = {"dams-manager-admin"};
        assertArrayEquals("Wrong roles extracted: " + String.join(", ", actualRoles), expectedRoles, actualRoles.toArray());
    }
}
